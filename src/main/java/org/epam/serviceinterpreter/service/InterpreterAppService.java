package org.epam.serviceinterpreter.service;

import org.epam.serviceinterpreter.exception.InterpreterLangUnknownException;
import org.epam.serviceinterpreter.exception.InterpreterLoadKeyboardInputException;
import org.epam.serviceinterpreter.exception.InterpreterLoadProgramException;
import org.epam.serviceinterpreter.exception.InterpreterProgramExecutionException;
import org.epam.serviceinterpreter.service.InterpreterFactory;
import org.epam.serviceinterpreter.service.Interpreter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class InterpreterAppService {
    private InterpreterFactory interpreterFactory;

    @Autowired
    public InterpreterAppService(InterpreterFactory interpreterFactory){
        this.interpreterFactory=interpreterFactory;
    }

    public String execute(String lang, String programText, String passedKeys) throws
            InterpreterLangUnknownException,InterpreterLoadProgramException,InterpreterLoadProgramException,InterpreterLoadKeyboardInputException,InterpreterProgramExecutionException {
        Interpreter interpreter = interpreterFactory.createInterpreter(lang);
        return executeWithInterpreter(interpreter, programText, passedKeys);
    }

    private String executeWithInterpreter(Interpreter interpreter, String programText, String passedKeys) throws
            InterpreterLoadProgramException,InterpreterLoadKeyboardInputException,InterpreterProgramExecutionException {
        try{
            interpreter.loadProgram(programText);
        }catch (Exception e){
            throw new InterpreterLoadProgramException(programText);
        }
        try{
            interpreter.loadKeyboardInput(passedKeys);
        }catch (Exception e){
            throw new InterpreterLoadKeyboardInputException(passedKeys);
        }
        try{
            while(interpreter.hasNextCommand()){
                interpreter.executeNextCommand();
            }
        }catch (Exception e){
            throw new InterpreterProgramExecutionException();
        }
        return interpreter.getResult();
    }
}
