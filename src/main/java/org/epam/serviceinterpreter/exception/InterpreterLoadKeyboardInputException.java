package org.epam.serviceinterpreter.exception;

public class InterpreterLoadKeyboardInputException extends RuntimeException {
    public InterpreterLoadKeyboardInputException(String msg){
        super(msg);
    }
}
