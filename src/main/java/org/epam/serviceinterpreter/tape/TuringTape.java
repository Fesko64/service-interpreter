package org.epam.serviceinterpreter.tape;

public interface TuringTape {
    void writeRowValue(char value);

    char getRowValue();

    void gotoNextRow();

    void gotoPreviousRow();

    void increaseRowValueByOne();

    void reduceRowValueByOne();
}
